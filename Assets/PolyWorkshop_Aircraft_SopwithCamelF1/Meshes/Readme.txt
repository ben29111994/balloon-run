This folder contains models with colliders.

[Aircraft_SopwithCamelF1_Bomb_Mesh] contains bomb model with capsule collider.
[Aircraft_SopwithCamelF1_Collider] contains mesh collider for airplane model.
[Aircraft_SopwithCamelF1_Mesh] contains airplane mesh in three versions od LOD.
[Aircraft_SopwithCamelF1_Propeller_Mesh] contains propeller model (static and blurred)

