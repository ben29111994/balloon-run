Hi!
Thank you for purchasing SopwithCamelF1 asset.

I will explain everything you must know about this package.
In the beginning I would point out that if you want controll your airplane, you must import:
 1. Vehicles from Standard Assets package
 2. Cameras from Standard Assets package
 3. CrossPlatformInput from Standard Assets package (if any errors occured, delete MobileControlRig.cs from /Standard Assets/CrossPlatformInput/Scripts/)
If anything doesnt work, make sure you imported this packages before you import this asset.
If everything goes well, scene FlyingDemo shoud work correctly and you shoud be able to control your airplane.

...

In this package are three color versions of airplane: blue, green and yellow.
Each of them has two prefabs: Static and Flying. 
Flying containg airplane controll system, from Vehicles (Standard Assets) package.
Static versions is just model with materials, LODs, collisions.

The airplane model is separated into several parts:
- Body (Fuselage, not animated) 
- Cockpit (Only for third person view, low detailed cockpit. Not animated)
- 4 Ailerons, 2 Elevators, Rudder (Animated)
- Engine and Propeller (Animated)
- Lines (Not animated)

Each one of this parts has individual pivot, so you can rotate this parts.

The Airplane model has also 4 versions od LOD:
LOD0 - ~28k tris
LOD1 - ~15k tris
LOD2 - ~8k tris
LOD3 - ~4k tris

LOD1, LOD2 and LOD3 version can be successfully used in mobile projects.

As I said before, the airplane model has three color versions. If you want to create your own version, you can use photoshop project files from:
/Materials/Textures/PSD_Files
Remember to change mask of color both in Albedo and Specular map.
Gloss map and Normal map is common for all color versions.

This is all what you need to know.
If you have any questions, please send me message:
marcin.dyjak.b@gmail.com.

I'll try to help you.


Good luck!