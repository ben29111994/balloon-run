This folder contains prefabs.

[Aircraft_SopwithCamelF1_Bomb] is prefab of bomb with capsule-collider.
[Aircraft_SopwithCamelF1_Flying_Blue/Green/Yellow] is prefab of player controlled airplane. Working only with imported Vechicles from Standard Assets package.
[Aircraft_SopwithCamelF1_Static_Blue/Greeen/Yellow] is prefab of static version of airplane. Contains only model with separated animated parts, materials, LOD versions and colliders.
