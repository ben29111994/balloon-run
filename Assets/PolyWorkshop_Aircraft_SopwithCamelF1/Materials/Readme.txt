This folder contains materials and textures for airplanes and environment.

[Aircraft_SopwithCamelF1_BodyBlue]
[Aircraft_SopwithCamelF1_BodyGreen]
[Aircraft_SopwithCamelF1_BodyYellow]
is materials for three color versions of airplane.

[Aircraft_SopwithCamelF1_Cockpit] is cockpit material (common for all color versions)

Files whose name start with [Environment] is materials for skybox and environment elements on scenes.

[Textures] folder contains maps for materials of airplane: Albedo (3 versions), Specular(3 versions), Gloss (1 versions) and Normal (1 versions).

[Aircraft_SopwithCamelF1_PropellerBlur_xxx] - three versions of blured propeller in two colors: gray and blue.

